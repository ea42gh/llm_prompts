# When the system misbehaves
* ask it to **List the steps**
* ask it to **How ig got to the current step**
* tell it to **go to step #**

# Coding Help
* Coding/CodingExpert.md
* Coding/JuliaExpert.md
 
# Language Learning
* Lang/ParáfrasisEspañol.md
* Lang/StoryCreator.md
* Lang/TravelGuide.md
* Lang/fooTravelGuide.md
 
# Math Learning
* Learn/LinearAlgebraExpert.md
* Learn/TeachMe.md
 
# Useful Snippets
* Snippets/Pick_Lang_and_Level.md
